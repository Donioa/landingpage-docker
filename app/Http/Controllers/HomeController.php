<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\price_list;
class HomeController extends Controller
{
    //
	public function index(){
		$price_list = price_list::get();
		return view('landingpage',array('price_list'=>$price_list));
	}
}
