<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>FD Test</title>
		<link rel="shortcut icon" href="https://www.niagahoster.co.id/assets/images/2019/favicon.png">
        
		 <!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		
		<!-- Scripts -->
		<script src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
		<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
		<script src="{{ asset('js/jquery.easing/jquery.easing.min.js') }}"></script>
		<!-- Style -->
		<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/all.css') }}" rel="stylesheet">
		<link href="{{ asset('css/frontend.css') }}" rel="stylesheet">
    </head>
	<body>
	<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
	<div class="container d-flex">
      <div class="contact-info mr-auto">
        <a href="#"><i class="fa fa-tags"></i> Gratis E-Book 9 cara cerdas menggunakan domain </a>
      </div>
      <div class="social-links">
        <a href="tel:0274-53055505" class="phone"><i class="fa fa-phone"></i> 0274-53055505</a>
		<a href="#" class="chat"><i class="fa fa-comments" ></i> Life Chat</a>
		<a href="#" class="member"><i class="fa fa-user-circle"></i> Member Area</a>
      </div>
    </div>
  </div>
	
	<header class="fixed-top" id="header">
	
	<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark primary-color">

  <div class="container">

    <!-- Navbar brand -->
    <a class="navbar-brand" href="/">
		<img src="{{ asset('images/logo.png') }}" class="img-responsive"/>
	</a>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
      aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon ">
	  
	  </span>
    </button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="basicExampleNav">

      <!-- Links -->
      <ul class="navbar-nav ml-auto">
       <li class="nav-item active"><a href="#hosting" class="nav-link">Hosting</a></li>
	          <li class="nav-item"><a href="#domain" class="nav-link">Domain</a></li>
	          <li class="nav-item"><a href="#server" class="nav-link">Server</a></li>
	          <li class="nav-item"><a href="#website" class="nav-link">Website</a></li>
	          <li class="nav-item"><a href="#afiliasi" class="nav-link">Afiliasi</a></li>
	          <li class="nav-item cta"><a href="#promo" class="nav-link">Promo</a></li>
			  <li class="nav-item cta"><a href="#payment" class="nav-link">Pembayaran</a></li>
		      <li class="nav-item cta"><a href="#review" class="nav-link">Review</a></li>
			  <li class="nav-item cta"><a href="#contact" class="nav-link">Kontak</a></li>
			  <li class="nav-item cta"><a href="#blog" class="nav-link">Blog</a></li>
      </ul>
      <!-- Links -->
    </div>
    <!-- Collapsible content -->

  </div>
	
</nav>
<!--/.Navbar-->
</header>
<!--Main Navigation-->
<!--Main layout-->
<main id="content-page">
	<section class="hosting">
		<div class="container">
		<div id="hosting">
			<div class="col-md-6">
			<h3>PHP Hosting</h3>
				<p>Cepat Handal, penuh dengan modul PHP yang Anda Butuhkan</p>
			<ul class="hosting-list fa-ul">
				<li>
				<span class="fa-li">
				<i class="fas fa-check-circle"></i>
				</span>
				Solusi PHP untuk Performa query yang lebih cepat</li>
				<li>
				<span class="fa-li">
				<i class="fas fa-check-circle"></i>
				</span>
				Konsumsi memori yang lebih rendah</li>
				<li> 
				<span class="fa-li">
				<i class="fas fa-check-circle"></i>
				</span>
				Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6 PHP 7</li>
				<li> 
				<span class="fa-li">
				<i class="fas fa-check-circle"></i>
				</span>
				Fitur enkripsi IonCube dan Zen Guard Loaders</li>
			</ul>
			</div>
			
		</div>
		<div class="col-md-12 border-top" id="server">
		  <div class="row support-host">
			<div class="col-md-4 ">
				<div class="image-server">
					<object data="{{ asset('svg/icon PHP Hosting_zendguard.svg') }}" type="image/svg+xml" class="h-60"></object>
				</div>
				<p>PHP Zen Guard Loader</p>
			</div>
			<div class="col-md-4 ">
				<div class="image-server">
					<object data="{{ asset('svg/icon PHP Hosting_composer.svg') }}" type="image/svg+xml"></object>
				</div>
				<p>PHP Composer</p>
			</div>
			<div class="col-md-4 ">
				<div class="image-server">
					<object data="{{ asset('svg/icon PHP Hosting_ioncube.svg') }}" type="image/svg+xml"  class="h-60">></object>
				</div>
				<p>PHP IonCube Loader</p>
			</div>
		   </div>
		   <h4 class="font-weight-bold">Paket Hosting Singapura yang Tepat</h4>
		   <h4>Diskon 40% + Domain dan SSL Gratis untuk Anda</h4>
		</div>
	</div>
	</section>
	<section class="col-md-12 domain" id="domain">
	   <div class="container">
		<div class="row">
				 @foreach($price_list as $price_lists)
					@if($price_lists->fav==1)
						@php $addClass="featured"; @endphp
					@else
						@php $addClass=''; @endphp
					@endif
			<div class="col-lg-3 col-md-6" id="pricing">
				<div class="box {{$addClass}}">
				@if($price_lists->fav==1)
				<span class="favorite">Best Seller !</span>
				@endif
              <h3>{{$price_lists->name}}</h3>
			  
             <div class="pricing">
			  <sup class="text-line">@currency($price_lists->price)/ {{$price_lists->duration}} </sup>
			  <h4>
			 Rp.
			 @php
			 $number=number_format($price_lists->discount, 0, ',', '.'); 
			 $splitData=explode('.',$number);
			 @endphp
			 {{$splitData[0]}}.<sup style="font-size:14px;">{{$splitData[1]}}
			  /{{$price_lists->duration}}</sup></h4>
			 </div>
			 <p>{{ number_format($price_lists->used, 0, ',', '.')}} Pengguna Terdaftar</p>
				{!! $price_lists->description!!}
              <div class="btn-wrap">
                <a href="#" class="btn-buy">Pilih Sekarang</a>
              </div>
            </div>
			</div>
				@endforeach
		</div>
		</div>
	</section>
	<section class="col-md-12 website" id="website">
	   <div class="container">
	   <div class="section-title">
          <h3>Powerful dengan Limit PHP yang Lebih Besar</h3>
        </div>
		<div class="row linkinfo">
			<div class="col-md-6">
			<div class="info-list">
				<i class="fas fa-check-circle"></i> max execution 300s
			</div>
			<div class="info-list dark">
				<i class="fas fa-check-circle"></i> max execution 300s
			</div>
			<div class="info-list">
				<i class="fas fa-check-circle"></i> php memory limit 1024 MB
			</div>			
			</div>
			<div class="col-md-6">
			<div class="info-list">
				<i class="fas fa-check-circle"></i> php max size 128MB
			</div>
			<div class="info-list dark">
				<i class="fas fa-check-circle"></i> upload max filesize 128MB
			</div>
			<div class="info-list">
				<i class="fas fa-check-circle"></i> max input vars 2500
			</div>			
			</div>
		</div>
		
	   <div>
	</section>
	<section class="fasility">
	<div class="container">
	   <div class="section-title">
          <h3>Semua Paket Hosting Sudah Termasuk</h3>
        </div>
		<div class="row">
			<div class="col-md-4">
				<div class="php-versions">
				<img src="{{ asset('svg/icon PHP Hosting_PHP Semua Versi.svg') }}" class="img-fluid"/>
					<h4>PHP Semua Versi</h4>
					<p>Pilih mulai dari versi PHP 5.3 s/d PHP 7</p>
					<p>Ubah sesuka Anda !</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="php-versions">
				<img src="{{ asset('svg/icon PHP Hosting_My SQL.svg') }}" class="img-fluid"/>
					<h4>MySQL Versi 5.6</h4>
					<p>Nikmati MySQL versi terbaru,tercepat dan</p>
					<p>kaya akan fitur</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="php-versions">
				<img src="{{ asset('svg/icon PHP Hosting_CPanel.svg') }}" class="img-fluid"/>
					<h4>Panel Hosting cPanel</h4>
					<p>Kelola website dengan panel canggih yang</p>
					<p>familiar di hati Anda.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="php-versions">
				<img src="{{ asset('svg/icon PHP Hosting_garansi uptime.svg') }}" class="img-fluid"/>
					<h4>Garansi Uptime 99.9%</h4>
					<p>Data center yang mendukung kelangsingan</p>
					<p>website Anda 24/7</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="php-versions">
				<img src="{{ asset('svg/icon PHP Hosting_InnoDB.svg') }}" class="img-fluid"/>
					<h4>Database InnoDB Unlimeted</h4>
					<p>Jumlah dan ukuran database yang tumbuh</p>
					<p>sesuai kebutuhan Anda.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="php-versions">
				<img src="{{ asset('svg/icon PHP Hosting_My SQL remote.svg') }}" class="img-fluid"/>
					<h4>Wildcard Remote MySQL</h4>
					<p>Mendukung s/d 25 max_user_connections</p>
					<p>dan 100 max_connections</p>
				</div>
			</div>
		</div>
	</div>
	</section>
	<section class="framework">
	<div class="container">
		<div class="section-title">
          <h3>Mendukung Penuh Framework Laravel</h3>
        </div>
		<div class="framework-info">
			<div class="col-md-6">
			<h4>Tak perlu menggunakan dedicated server ataupun VPS
			yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda
			</h4>
			<ul class="fa-ul">
				<li>
				<span class="fa-li">
				<i class="fas fa-check-circle"></i>
				</span>
				Install Laravel 1 klik dengan Softaculous Installer.
				</li>
				<li>
				<span class="fa-li">
				<i class="fas fa-check-circle"></i>
				</span>
				Mendukung ekstensi <b>PHP MCrypt,phar,mbstring dan fileinfo</b>
				</li>
				<li>
				<span class="fa-li">
				<i class="fas fa-check-circle"></i>
				</span>
				Tersedia <b> Composer </b> dan <b>SSH</b> untuk menginstal packages pilihan Anda. 
				
				</li>
			</ul>
			<span> Nb : Composer dan SSH hanya tersedia pada paket Personal dan Bisnis</span>
			<div class="btn-wrap">
                <a href="#" class="btn-buy">Pilih Hosting Anda</a>
            </div>
			</div>
		</div>
		</div>
	</section>
	<section class="moduls">
		<div class="container">
		<div class="section-title">
          <h3>Modul Lengkap untuk Menjalankan Aplikasi PHP Anda.</h3>
        </div>
		<div class="row moduls-info">
			<div class="col-md-3">
				<p>IcePhp</p>
			</div>
			<div class="col-md-3">
				<p>http</p>
			</div>
			<div class="col-md-3">
				<p>nd_pdo_mysql</p>
			</div>
			<div class="col-md-3">
				<p>stats</p>
			</div>
			<div class="col-md-3">
				<p>apc</p>
			</div>
			<div class="col-md-3">
				<p>huffman</p>
			</div>
			<div class="col-md-3">
				<p>oauth</p>
			</div>
			<div class="col-md-3">
				<p>stem</p>
			</div>
			<div class="col-md-3">
				<p>apcu</p>
			</div>
			<div class="col-md-3">
				<p>idn</p>
			</div>
			<div class="col-md-3">
				<p>oci8</p>
			</div>
			<div class="col-md-3">
				<p>stomp</p>
			</div>
			<div class="col-md-3">
				<p>apm</p>
			</div>
			<div class="col-md-3">
				<p>igbinary</p>
			</div>
			<div class="col-md-3">
				<p>odbc</p>
			</div>
			<div class="col-md-3">
				<p>suhosin</p>
			</div>
			<div class="col-md-3">
				<p>ares</p>
			</div>
			<div class="col-md-3">
				<p>imagick</p>
			</div>
			<div class="col-md-3">
				<p>opcache</p>
			</div>
			<div class="col-md-3">
				<p>sybase_ct</p>
			</div>
			<div class="col-md-3">
				<p>bcmath</p>
			</div>
			<div class="col-md-3">
				<p>imap</p>
			</div>
			<div class="col-md-3">
				<p>pdf</p>
			</div>
			<div class="col-md-3">
				<p>sysvmsg</p>
			</div>
			<div class="col-md-3">
				<p>bcompiler</p>
			</div>
			<div class="col-md-3">
				<p>include</p>
			</div>
			<div class="col-md-3">
				<p>pdo</p>
			</div>
			<div class="col-md-3">
				<p>sysvsem</p>
			</div>
			<div class="col-md-3">
				<p>big_int</p>
			</div>
			<div class="col-md-3">
				<p>inotify</p>
			</div>
			<div class="col-md-3">
				<p>pdo_dblib</p>
			</div>
			<div class="col-md-3">
				<p>sysvshm</p>
			</div>
			<div class="col-md-3">
				<p>bitset</p>
			</div>
			<div class="col-md-3">
				<p>interbase</p>
			</div>
			<div class="col-md-3">
				<p>pdo_firebird</p>
			</div>
			<div class="col-md-3">
				<p>tidy</p>
			</div>
			<div class="col-md-3">
				<p>bloomy</p>
			</div>
			<div class="col-md-3">
				<p>intl</p>
			</div>
			<div class="col-md-3">
				<p>pdo_mysql</p>
			</div>
			<div class="col-md-3">
				<p>timezoneonedb</p>
			</div>
			<div class="col-md-3">
				<p>bz2_filter</p>
			</div>
			<div class="col-md-3">
				<p>ioncuve_loader</p>
			</div>
			<div class="col-md-3">
				<p>pdo_odbc</p>
			</div>
			<div class="col-md-3">
				<p>trader</p>
			</div>
			
			<div class="col-md-3">
				<p>clamav</p>
			</div>
			<div class="col-md-3">
				<p>ioncube_loader_4</p>
			</div>
			<div class="col-md-3">
				<p>pdo_pgsql</p>
			</div>
			<div class="col-md-3">
				<p>translit</p>
			</div>
			<div class="col-md-3">
				<p>coin_acceptor</p>
			</div>
			<div class="col-md-3">
				<p>jsmin</p>
			</div>
			<div class="col-md-3">
				<p>pdo_sqlite</p>
			</div>
			<div class="col-md-3">
				<p>uploadprogress</p>
			</div>
			<div class="col-md-3">
				<p>crack</p>
			</div>
			<div class="col-md-3">
				<p>json</p>
			</div>
			<div class="col-md-3">
				<p>pgsql</p>
			</div>
			<div class="col-md-3">
				<p>uri_template</p>
			</div>
			<div class="col-md-3">
				<p>dba</p>
			</div>
			<div class="col-md-3">
				<p>ldap</p>
			</div>
			<div class="col-md-3">
				<p>phalcon</p>
			</div>
			<div class="col-md-3">
				<p>uuid</p>
			</div>
			<div class="btn-wrap">
                <a href="#" class="btn-buy">Selengkapnya</a>
            </div>
		</div>
	</section>
	<section class="linux-hosting">
	<div class="container">
		<div class="linux-hosting-info">
			<div class="col-md-6">
			<h4>
			Linux Hosting yang Stabil dengan Teknologi LVE
			</h4>
			<p>
			SuperMicro <b>Intel Xeon 24-Cores</b> server dengan RAM <b>128GB</b>
			dan teknologi <b>LVE CloudLinux</b> untuk stabilitas server Anda. Dilengkapi
			dengan <b>SSD</b> untuk kecepatan <b>MySQL</b> dan caching. Apache load balancer
			berbasis LifeSpeed Technologies, <b>CageFS</b> security <b>Raid-10</b> protection
			dan auto backup untuk keamanan website PHP Anda
			</p>
			<div class="btn-wrap">
                <a href="#" class="btn-buy">
				Pilih Hosting Anda
				</a>
            </div>
			</div>
		</div>
		</div>
	</section>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="footer">
<div class="container">
	<div class="footer-info">
	<div class="row">
	<div class="col-md-6">
		<p>Bagikan jika Anda menyukai halaman ini.</p>
	</div>
		<div class="col-md-6">
			<div class="social-shared">
				<i class="fab fa-facebook-square"></i>
				<input type="text" value="80K"/>
			</div>
			<div class="social-shared">
			<i class="fab fa-twitter-square"></i>
				<input type="text" value="450"/>
			</div>
			<div class="social-shared">
			<i class="fab fa-google-plus-square"></i>
				<input type="text" value="1900"/>
			</div>
			
		</div>	
	</div>
	</div>
</div>
<div class="footer-contact">
	<div class="container">
		<div class="row">
		<div class="col-md-8">
		<h4>
		<b>Perlu Bantuan ?</b>Hubungi Kami : 0274-5305505
		</h4>
	       </div>
	       <div class="col-md-4">
	         <a href="#" class="chat"><i class="fa fa-comments" ></i> Life Chat</a>
	       </div>
	       </div>
	</div>
</div>
<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h4>HUBUNGI KAMI</h4>
					 <p>0274-5305505</p>
					 <p>Senin-Minggu</p>
					 <p>24 Jam Nonstop</p>
					 <br>
					 <p>
					 Jl. Selokan Mataram Monjali</p>
					 <p>Karangjati MT i/304</p>
					 <p>Sinduadi, Mlati, Sleman</p>
					 <p>Yogyakarta 55284
					 </p>

				</div>
				<div class="col-md-3">
					<h4>LAYANAN</h4>
					<ul>
					<li><a href="#">Domain</a></li>
					<li><a href="#">Shared Hosting</a></li>
					<li><a href="#">Cloud VPS Hosting</a></li>
					<li><a href="#">Manage VPS Hosting</a></li>
					<li><a href="#">Domain</a></li>
					<li><a href="#">Web Builder</a></li>
					<li><a href="#">Keamanan SSL / HTTPS</a></li>
					<li><a href="#">Jasa Pembuatan Website</a></li>
					<li><a href="#">Program Affiliasi</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h4>SERVICE HOSTING</h4>
					<ul>
					<li><a href="#">Hosting Murah</a></li>
					<li><a href="#">Hosting Indonesia</a></li>
					<li><a href="#">Hosting Singapura SG</a></li>
					<li><a href="#">Hosting PHP</a></li>
					<li><a href="#">Hosting Wordpress</a></li>
					<li><a href="#">Hosting Laravel</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h4>TUTORIAL</h4>
					<ul>
					<li><a href="#">Knowledgebase</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Cara Pembayaran</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h4>TENTANG KAMI</h4>
					<ul>
					<li><a href="#">Tim Niagahoster</a></li>
					<li><a href="#">Karir</a></li>
					<li><a href="#">Events</a></li>
					<li><a href="#">Penawaran & Promo Spesial</a></li>
					<li><a href="#">Kontak Kami</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<h4>KENAPA PILIH NIAGAHOSTER?</h4>
					<p>Support Terbaik</p>
					<p>Garansi Harga Termurah</p>
					<p>Domain Gratis Selamanya</p>
					<p>Datacenter Hosting Terbaik</p>
					<p>Review Pelanggan</p>
				</div>
				<div class="col-md-3">
					<h4>NEWSLETTER</h4>
					<div class="input-group custom-search">
					   <input type="text" class="form-control custom-search-input" placeholder="Email">
					   <button class="btn btn-primary custom-search-botton" type="submit">Berlangganan</button>  
					 </div>
					 <span>Dapatkan promo dan konten menarik dari penyedia hosting terbaik Anda.</span>
				</div>
				<div class="col-md-2">
				<i class="fab fa-facebook"></i>
				<i class="fab fa-twitter"></i>
				<i class="fab fa-google-plus"></i>
				</div>
				<div class="col-md-12">
				<h4>Pembayaran</h4>
					<ul class="logo-bank">
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/logo-bni-bordered.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/logo-bni-bordered.svg" alt="bank bni" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/logo-bca-bordered.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/logo-bca-bordered.svg" alt="bank bca" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/bri.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/bri.svg" alt="bank bri" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/mandiriclickpay.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/mandiriclickpay.svg" alt="mandiri clickpay" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/permatabank.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/permatabank.svg" alt="bank permata" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/atmbersama.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/atmbersama.svg" alt="atm bersama" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/prima.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/prima.svg" alt="prima" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/alto.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/alto.svg" alt="alto" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/visa.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/visa.svg" alt="visa" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/mastercard.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/mastercard.svg" alt="master card" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/indomaret.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/indomaret.svg" alt="indomaret" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/paypal.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/paypal.svg" alt="paypal" class="bank-logo">
</li>
<li>
<img src="https://www.niagahoster.co.id/assets/images/new_design/logo-gopay.svg" data-src="https://www.niagahoster.co.id/assets/images/new_design/logo-gopay.svg" alt="paypal" class="bank-logo">
</li>
</ul>
<p class="payment nunito-light note">Aktivasi instan dengan e-Payment. Hosting dan <a href="https://www.niagahoster.co.id/domain-murah" class="internal-link">domain</a> langsung aktif!</p>
				</div>
			</div>
		</div>
</div>
<div class="copyright_info">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12">
<hr class="super-soft">
</div>
<div class="col-md-8 col-sm-12 nunito-light">
Copyright ©2021 Niagahoster | Hosting powered by PHP8,
CloudLinux, CloudFlare, BitNinja and <a href="https://www.niagahoster.co.id/datacenter-hosting" class="internal-link">DC DCI-Indonesia</a><br>
Cloud <a href="https://www.niagahoster.co.id/cloud-vps-hosting" class="internal-link">VPS Murah</a> powered by Webuzo
Softaculous, Intel SSD and cloud computing technology
</div>
<div class="col-md-4 col-sm-12 col-xs-12 font-white center-text">
<span class="syarat-dan-ketentuan nunito-light">
<a href="https://www.niagahoster.co.id/syarat-dan-ketentuan">Syarat dan Ketentuan</a> | <a href="https://www.niagahoster.co.id/kebijakan-privasi">Kebijakan Privasi</a>
</span>
</div>
</div>
<a href="#" class="scrollup">&nbsp;</a>
</div>
</div>
</footer>
</body>
</html>
