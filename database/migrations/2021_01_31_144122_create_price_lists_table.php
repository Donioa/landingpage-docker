<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('name');
			$table->double('price');
			$table->double('discount');
			$table->enum('duration',array('bln','tahun'))->nullable();
			$table->text('description')->nullable(); 
			$table->integer('fav')->default(0)->comment('0-standart,1-bestseller');
            $table->integer('used')->default(0);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_lists');
    }
}
