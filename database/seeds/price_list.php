<?php

use Illuminate\Database\Seeder;
use App\price_list;

class price_list extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		$dataPrice_list = array(
		
		array('name' => 'Bayi',
                'price' => '19900',
                'discount' => '14900',
                'duration' => 'bln',
				'description' => '<ul>\r\n<li><b>0.5X RESOURCE POWER</b></li>\r\n<li><b>500MB</b> Disk Space</li>\r\n<li><b>Unlimited</b> Bandwidth</li>\r\n<li><b>Unlimited</b> Databases</li>\r\n<li><b>1</b> Domain</li>\r\n<li><b>Instant</b> Backup</li>\r\n<li><b>Unlimited SSL</b> Gratis Selamanya</li>\r\n</ul>\r\n',
				'used' => '4168',
				'fav'=>'0',
            ),
			array('name' => 'Pelajar',
                'price' => '46900',
                'discount' => '23450',
                'duration' => 'bln',
				'description' => '<ul>\r\n<li><b>1X RESOURCE POWER</b></li>\r\n<li><b>Unlimited</b> Disk Space</li>\r\n<li><b>Unlimited</b> Bandwidth</li>\r\n<li><b>Unlimited</b> POP3 Email</li>\r\n<li><b>Unlimited</b> Databases</li>\r\n<li><b>10</b>Addon Domain</li>\r\n<li><b>Instant</b> Backup</li>\r\n<li><b>Unlimited SSL</b> Gratis Selamanya</li>\r\n</ul>\r\n',
				'used' => '4168',
				'fav'=>'0',
            ),
			array('name' => 'Personal',
                'price' => '58900',
                'discount' => '38900',
                'duration' => 'bln',
				'description' => '<ul>\r\n<li><b>2X RESOURCE POWER</b></li>\r\n<li><b>Unlimited</b> Disk Space</li>\r\n<li><b>Unlimited</b> Bandwidth</li>\r\n<li><b>Unlimited</b> POP3 Email</li>\r\n<li><b>Unlimited</b> Databases</li>\r\n<li><b>Unlimited</b> Addon Domain</li>\r\n<li><b>Instant</b> Backup</li>\r\n<li><b>Domain Gratis</b> Selamanya</li>\r\n<li><b>Unlimited SSL</b> Gratis Selamanya</li>\r\n<li><b>Private</b> Name Server</li>\r\n<li><b>SpamAssain</b> Mail Protection</li>\r\n</ul>\r\n',
				'used' => '10017',
				'fav'=>'1',
            ),
			array('name' => 'Bisnis',
                'price' => '109900',
                'discount' => '65900',
                'duration' => 'bln',
				'description' => '<ul>\r\n<li><b>3X RESOURCE POWER</b></li>\r\n<li><b>Unlimited</b> Disk Space</li>\r\n<li><b>Unlimited</b> Bandwidth</li>\r\n<li><b>Unlimited</b> POP3 Email</li>\r\n<li><b>Unlimited</b> Databases</li>\r\n<li><b>Unlimited</b> Addon Domain</li>\r\n<li><b>Magic Auto</b> Backup & Restore </li>\r\n<li><b>Domain Gratis</b> Selamanya</li>\r\n<li><b>Unlimited SSL</b> Gratis Selamanya</li>\r\n<li><b>Private</b> Name Server</li>\r\n<li><b>Prioritas</b> Layanan Support </br>\r\n<i class=\"fa fa-star star-blue\"></i>\r\n<i class=\"fa fa-star star-blue\"></i>\r\n<i class=\"fa fa-star star-blue\"></i>\r\n<i class=\"fa fa-star star-blue\"></i>\r\n<i class=\"fa fa-star star-blue\"></i>\r\n</li>\r\n<li><b>SpamExpertt</b> Mail Protection</li>\r\n</ul>\r\n',
				'used' => '3552',
				'fav'=>'0',
            ),
        );
        DB::table('price_lists')->insert($dataPrice_list);
    }
}
